#include <string>

bool isnumber(const std::string &s)
{
	unsigned long l;
	const char *str;

	if (s.empty())
		return false;
	str = s.c_str();
	for (l = 0; str[l]; l++)
	{
		if (!std::isdigit(str[l]))
			return false;
	}
	return true;
}

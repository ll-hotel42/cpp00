#include "PhoneBook.hpp"
#include <cstring>

PhoneBook::PhoneBook(void)
{
	contact_nb = 0;
}

PhoneBook::PhoneBook(const PhoneBook &phonebook)
{
	for (long i = 0; i < contact_nb; i++)
		contacts[i] = phonebook.contacts[i];
}

PhoneBook::~PhoneBook(void) {}

long
PhoneBook::get_contact_nb(void) const
{
	return contact_nb;
}

const Contact *
PhoneBook::get_contact(long id) const
{
	if (id >= 0 && id < contact_nb)
		return &contacts[id];
	return NULL;
}

void
PhoneBook::add_contact(const Contact &contact)
{
	long i;

	if (contact_nb == 8)
	{
		for (i = 0; i < 7; i++)
		{
			contacts[i] = contacts[i + 1];
		}
		contacts[7] = contact;
	}
	else
		contacts[contact_nb++] = contact;
}

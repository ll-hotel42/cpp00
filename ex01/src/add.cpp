#include "PhoneBook.hpp"
#include <iostream>

void
add(PhoneBook &phonebook)
{
	Contact contact;

	std::cout << "Enter contact first name: ";
	std::getline(std::cin, contact.first_name);

	std::cout << "Enter contact last name: ";
	std::getline(std::cin, contact.last_name);

	std::cout << "Enter contact nickname: ";
	std::getline(std::cin, contact.nickname);

	std::cout << "Enter contact phone number: ";
	std::getline(std::cin, contact.phone_number);
	while (!isnumber(contact.phone_number))
	{
		std::cout << "Error: `" << contact.phone_number << "' is not a valid number" << std::endl;
		std::cout << "Please enter a valid phone number: ";
		std::getline(std::cin, contact.phone_number);
	}

	std::cout << "Enter contact darkest secret: ";
	std::getline(std::cin, contact.darkest_secret);

	phonebook.add_contact(contact);
}

#include "PhoneBook.hpp"
#include <algorithm>
#include <cmath>
#include <iostream>
#include <cstdlib>

static void display_contact_info(const std::string &info);

void
search(const PhoneBook &phonebook)
{
	const Contact	*contact;
	std::string		input;
	long			i;
	long			index_spacing;

	if (phonebook.get_contact_nb() == 0)
	{
		std::cout << "No contact registered" << std::endl;
		return ;
	}
	for (i = 0; (contact = phonebook.get_contact(i)); i++)
	{
		index_spacing = 9 - (i > 0 ? std::log10(i) : 1);
		while (index_spacing-- > 0)
			std::cout << ' ';
		std::cout << i << " | ";

		display_contact_info(contact->first_name);
		std::cout << " | ";
		display_contact_info(contact->last_name);
		std::cout << " | ";
		display_contact_info(contact->nickname);
		std::cout << std::endl;
	}
	std::cout << "Enter contact index: ";
	do
	{
		std::getline(std::cin, input);
		i = std::atol(input.c_str());
	}
	while (!isnumber(input) && \
			(i < 0 || i >= phonebook.get_contact_nb()));

	contact = phonebook.get_contact(i);
	std::cout << contact->first_name << std::endl;
	std::cout << contact->last_name << std::endl;
	std::cout << contact->nickname << std::endl;
	std::cout << contact->phone_number << std::endl;
	std::cout << contact->darkest_secret << std::endl;
}

static void display_contact_info(const std::string &info)
{
	long index_spacing;
	long j;

	index_spacing = std::min((size_t)8, info.length());
	for (j = 0; j < 8 - index_spacing; j++)
	{
		std::cout << ' ';
	}
	for (j = 0; j < index_spacing - 1; j++)
	{
		std::cout << info.at(j);
	}
	if (info.length() > 8)
		std::cout << '.';
	else
		std::cout << info.at(j);
}

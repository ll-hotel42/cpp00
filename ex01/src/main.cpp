#include "PhoneBook.hpp"
#include <iostream>
#include <string>

int	main(void)
{
	PhoneBook phonebook;
	std::string line;

	do {
		do
		{
			std::cout << "Enter command (ADD, SEARCH, EXIT): ";
			std::getline(std::cin, line);
		}
		while (!std::cin.eof() && line != "ADD" && line != "SEARCH" && line != "EXIT");

		if (line == "ADD")
		{
			add(phonebook);
		}
		else if (line == "SEARCH")
		{
			search(phonebook);
		}
	} while (!std::cin.eof() && line != "EXIT");
	return 0;
}

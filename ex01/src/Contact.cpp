#include "Contact.hpp"

Contact::Contact(void) {}

Contact::Contact(const Contact &contact)
{
	first_name = contact.first_name;
	last_name = contact.last_name;
	nickname = contact.nickname;
	phone_number = contact.phone_number;
	darkest_secret = contact.darkest_secret;
}

Contact::~Contact(void) {}

Contact
Contact::operator=(const Contact &contact)
{
	first_name = contact.first_name;
	last_name = contact.last_name;
	nickname = contact.nickname;
	phone_number = contact.phone_number;
	darkest_secret = contact.darkest_secret;
	return (*this);
}

#ifndef PHONEBOOK_HPP
# define PHONEBOOK_HPP

# include "Contact.hpp"

class PhoneBook
{
public:
	PhoneBook(void);
	PhoneBook(const PhoneBook &);
	~PhoneBook(void);

	long get_contact_nb(void) const;
	const Contact *get_contact(long id) const;
	void add_contact(const Contact &);

private:
	long contact_nb;
	Contact contacts[8];
};

void add(PhoneBook &);
void search(const PhoneBook &);
bool isnumber(const std::string &s);

#endif

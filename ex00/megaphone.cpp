#include <iostream>
#include <sys/types.h>

int
main(int argc, char **argv)
{
	int		i;

	if (argc < 2)
	{
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
	}
	else
	{
		for (i = 1; i < argc; i++)
		{
			for (u_long l = 0; argv[i][l]; l++)
			{
				argv[i][l] = std::toupper(argv[i][l]);
				std::cout << argv[i][l];
			}
		}
		std::cout << std::endl;
	}
	return 0;
}
